package main

import "fmt"

func lift(id int, rx <-chan Message, tx chan<- Message) {
    // specs
    var pos = 0
    var target = pos
    var pax  = 0
    var paxMax = 5
    var status = "wait" // wait, ride

    // metrics
    var steps = 0

    // loop
    for {
        select {
        case msg := <-rx:
            switch msg.cmd {

            case "step":
                if pos < target {
                    status = "ride"
                    pos++
                    fmt.Println("Lift", id, "step : go up to", target, "now on ", pos)
                } else if pos > target {
                    status = "ride"
                    pos--
                    fmt.Println("Lift", id, "step : go down to", target, "now on ", pos)
                }
                if pos == target && status != "wait" {
                    status = "wait"
                    tx <- Message{255, id, "pling", []int{pos}}
                    fmt.Println("Lift", id, "pling at", pos)
                }
                tx <- Message{0, id, "done", nil}

            case "newTarget":
                if msg.dst == id && msg.args[0] != target {
                    target = msg.args[0]
                    fmt.Println("Lift", id, "new target", target)
                }

            case "enterRequest":
                if pax < paxMax {
                    fmt.Println("Lift", id, "enterRequest", "ok")
                    pax++
                    target = msg.args[1]
                    tx <- Message{msg.src, id, "enterResponse", []int{1}}
                } else {
                    fmt.Println("Lift", id, "enterRequest", "full")
                    tx <- Message{msg.src, id, "enterResponse", []int{0}}
                }

            case "success":
                fmt.Println("Lift", id, "success")
                if pax > 0 {
                    pax--
                }

            case "end":
                fmt.Println("Lift", id, "end")
                tx <- Message{msg.src, id, "end", []int{steps}}
                return
            }
        default:
        }
    }
}
