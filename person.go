package main

import (
    "fmt"
    "math/rand"
    "time"
)

func person(id int, tx chan<- Message, txRequest chan<- MessageRequest) {
    // specs
    var pos int
    var target int
    var status = "wait" // wait, enter, ride, success
    var lift = -1

    // metrics
    var steps = 0

    // generate
    rand.Seed(time.Now().Unix())
    pos = rand.Intn(10)
    target = rand.Intn(10)
    var rx = make(chan Message, 10)
    txRequest <- MessageRequest{0, id, pos, rx}
    fmt.Println("Person", id, "at", pos, "request target", target)

    // loop
    for {
        select {
        case msg := <-rx:
            switch msg.cmd {

            case "step":
                fmt.Println("Person", id, "step")
                steps++

            case "pling":
                switch status {

                case "wait":
                    if pos == msg.args[0] {
                        lift = msg.src
                        fmt.Println("Person", id, "pling from lift", lift, status, "try enter")
                        status = "enter"
                        tx <- Message{msg.src, id, "enterRequest", []int{pos, target}}
                    } else {
                        fmt.Println("Person", id, "pling", status, "wait")
                        tx <- Message{0, id, "done", nil}
                    }

                case "ride":
                    if msg.src == lift && msg.args[0] == target {
                        fmt.Println("Person", id, "pling", status, "success", target)
                        tx <- Message{msg.src, id, "success", []int{steps}}
                        return
                    } else {
                        fmt.Println("Person", id, "pling", status, target)
                        tx <- Message{0, id, "done", nil}
                    }
                }

            case "enterResponse":
                if status == "enter" && msg.src == lift {
                    if msg.args[0] == 1 {
                        status = "ride"
                    } else {
                        status = "wait"
                    }
                }
                fmt.Println("Person", id, "enterResponse", msg.args[0], status)
                tx <- Message{0, id, "done", nil}

            case "end":
                fmt.Println("Person", id, "end")
                tx <- Message{msg.src, id, "end", []int{steps}}
                return
            }
        default:
        }
    }
}

