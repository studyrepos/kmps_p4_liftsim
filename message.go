package main

type Message struct {
    dst  int
    src  int
    cmd  string
    args []int
}

type MessageRequest struct {
    dst int
    src int
    pos int
    rx  chan Message
}
