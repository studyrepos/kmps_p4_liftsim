package main

import "fmt"

func liftCtrl(id int, rx <-chan Message, tx chan<- Message, rxRequests <-chan MessageRequest) {

    // channels
    var rxLift = make(chan Message, 10)
    var rxPerson = make(chan Message, 10)

    // init txChannelMaps for Lifts and Persons
    var txLift = make(map[int]chan Message)
    var txPerson = make(map[int]chan Message)

    // specs
    var doneCnt = 0

    // metrics


    // init lifts
    for i := 1; i < 2; i++ {
        // create channel
        txLift[i] = make(chan Message, 1)
        // start lift
        go lift(i,txLift[i], rxLift)
    }

    // loop
    for {

        // return controller if no more Lifts and Persons listed
        if len(txLift) == 0 && len(txPerson) == 0 {
            tx <- Message{4242, 0, "allDone", nil}
            return
        }

        if doneCnt == len(txLift) + len(txPerson) {
            tx <- Message{4242, 0, "done", nil}
        }

        select {

        // receive a command from SimulationControl
        case msg := <-rx:
            switch msg.cmd {
            case "step", "end":
                fmt.Println("LiftCntrl", id, "step")
                // make one step
                for k, v := range txLift{
                    v <- Message{k, 0, msg.cmd, nil}
                }
                for k, v := range txPerson{
                    v <- Message{k, 0, msg.cmd, nil}
                }
            }


        // new Person, add channel to txPersonMap and plan ride to targets
        case msg := <-rxRequests:
                if txPerson[msg.dst] == nil {
                    txPerson[msg.dst] = msg.rx
                    // ToDo: plan ride over msg.pos
                    txLift[1] <- Message{1, 0, "newTarget", []int{msg.pos}}
                }

        // receive messages from lifts
        case msg := <-rxLift:
            switch msg.cmd {
            case "done":
                doneCnt++

            case "pling":
                for k, v := range txPerson {
                    v <- Message{k, id, "pling", msg.args}
                }

            case "end":
                fmt.Println("LiftCntrl", id, "end", "lift", msg.src)
                close(txLift[msg.src])
                delete(txLift, msg.src)
                tx <- msg

            case "enterResponse":
                txPerson[msg.dst] <- msg
            }

        // receive messages from Persons
        case msg := <-rxPerson:
            switch msg.cmd {
            case "done":
                doneCnt++

            case "success", "end":
                fmt.Println("LiftCntrl", id, "end", "Person", msg.src)
                close(txPerson[msg.src])
                delete(txPerson, msg.src)
                tx <- msg

            case "enterRequest":
                txLift[msg.dst] <- msg
            }

        default:
        }
    }
}
