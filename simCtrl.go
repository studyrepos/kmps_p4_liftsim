package main

import (
    "fmt"
)

func simCtrl(_steps int) {

    // specs
    const id int = 4242

    var steps = _steps
    var nextStep = true

    var nextPersonId = 100

    // channels
    var rx = make(chan Message, 5)
    var tx = make(chan Message, 5)

    var rxPersons = make(chan Message)
    var rxRequests = make(chan MessageRequest)

    // metrics


    // init
    go liftCtrl(0, tx, rx, rxRequests)


    tx <- Message{0, id, "step", nil}

    go person(nextPersonId, rxPersons, rxRequests)

    // loop
    for steps >= 0 {

        if steps == 0 {
            tx <- Message{0, id, "end", nil}
        }

        // new step, generate Persons, notify liftCtrl
        if nextStep {
            nextStep = false
            //// new Person ?
            //if rand.Intn(100) > 25 {
            //    go person(nextPersonId, rxPersons, rxRequests)
            //    nextPersonId++
            //
            //    //for i := 0; i < rand.Intn(5); i++ {
            //    //    go person(nextPersonId, rxPersons, rxRequests)
            //    //    nextPersonId++
            //    //}
            //}
        }


        select {
            case msg := <-rx:
                switch msg.cmd {
                case "done":
                    if steps != 0 {
                        steps--
                        nextStep = true
                        tx <- Message{0, id, "step", nil}
                    }

                case "success":
                    fmt.Println(msg)

                case "allDone":
                    return
                }
        default:
        }

    }

}
